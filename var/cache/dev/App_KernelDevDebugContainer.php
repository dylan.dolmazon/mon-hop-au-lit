<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerTvzrZ3T\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerTvzrZ3T/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerTvzrZ3T.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerTvzrZ3T\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerTvzrZ3T\App_KernelDevDebugContainer([
    'container.build_hash' => 'TvzrZ3T',
    'container.build_id' => 'e3171a66',
    'container.build_time' => 1675345004,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerTvzrZ3T');
